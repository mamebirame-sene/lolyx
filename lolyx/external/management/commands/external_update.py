#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output urls of external ressources
"""
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from lolyx.external.models import UserRessource
from lolyx.external.models import UserRessourceArchive
from lolyx.llx.models import UserExtra
from lolyx.external.utils.LyxExternalPlugin import LyxExternalPlugin
import json


class Command(BaseCommand):
    help = 'Update external information per user'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.api_userpage()
        self.api_updates()

    def api_updates(self):
        """Call apis

        - do not call api updates less than one hour

        """
        delta = 12
        dtn = datetime.now() - timedelta(hours=delta)
        ressources = UserRessource.objects.filter(api_last_status=200,
                                                  api_last_checked__lt=dtn,
                                                  ressource__api_active=True).order_by("-last_checked")
        for res in ressources:
            classname = res.ressource.classname

            module = __import__("lolyx.external.utils.plugins.{}".format(classname))
            class_ = getattr(module.external.utils.plugins, classname)
            final = getattr(class_, classname)
            plugin = final()

            extra, created = UserExtra.objects.get_or_create(user=res.user)
            if created:
                actual = {}
            else:
                actual = json.loads(extra.extra)

            (status, actual[res.ressource.name]) = plugin.fetch(res.api_url())
            actual[res.ressource.name]['url'] = res.userpage()
            actual[res.ressource.name]['api_status'] = status
            extra.extra = json.dumps(actual)
            extra.save()

            UserRessourceArchive.objects.create(ures=res,
                                                data=json.dumps(actual))

            res.api_last_status = status
            print res.api_url(), res.api_last_status, res.api_last_checked
            res.api_last_checked = datetime.now()

            res.save()

    def api_userpage(self):
        """Check the userpage
        """
        delta = 12
        dtn = datetime.now() - timedelta(hours=delta)
        ressources = UserRessource.objects.filter(last_status=200,
                                                  last_checked__lt=dtn).order_by("-last_checked")

        for res in ressources:

            extra, created = UserExtra.objects.get_or_create(user=res.user)
            if created:
                actual = {}
            else:
                actual = json.loads(extra.extra)

            lyx = LyxExternalPlugin()
            status = lyx.head(res.userpage())

            actual[res.ressource.name] = {'type': 'userpage',
                                          'url': res.userpage(),
                                          'status': status}
            extra.extra = json.dumps(actual)
            extra.save()

            res.last_status = status
            print res.userpage(), res.last_status, res.last_checked
            res.last_checked = datetime.now()

            res.save()

# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
OpenStreetMap plugin

<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="OpenStreetMap server">
  <user id="26348" display_name="Rodolphe Quiédeville" account_created="2008-02-04T12:40:59Z">
    <description>My wiki page : http://wiki.openstreetmap.org/wiki/User:Rodolphe</description>
    <contributor-terms agreed="true"/>
    <img href="http://www.gravatar.com/avatar/95a63eeb28d5e8237cfb8d6c75798ef1.jpg?s=256&amp;d=http%3A%2F%2Fwww.openstreetmap.org%2Fassets%2Fusers%2Fimages%2Flarge-8d2e51c2ddd01eb899f4bfb0bca3cf5e.png"/>
    <roles>
    </roles>
    <changesets count="1069"/>
    <traces count="14"/>
    <blocks>
      <received count="0" active="0"/>
    </blocks>
  </user>
</osm>
"""
from lxml import etree
from lolyx.external.utils.LyxExternalPlugin import LyxExternalPlugin


class LyxOpenStreetMap(LyxExternalPlugin):

    @classmethod
    def fetch(self, url):
        datas = {}
        (status, content) = self.download(url)
        if status == 200:
            datas = self.parse(content)
        return (status, datas)

    @classmethod
    def parse(self, data):
        """
        Return : json object

        data : xml string
        """
        traces = 0
        changesets = 0

        try:
            val = etree.XML(data)
            user = val.find("user")
            changesets = user.find("changesets").get('count')
            traces = user.find("traces").get('count')
        except:
            pass

        return {"type": "osm",
                "traces": int(traces),
                "changesets": int(changesets)}

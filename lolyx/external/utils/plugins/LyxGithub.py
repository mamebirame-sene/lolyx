# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Github plugin

{
  "login": "rodo",
  "id": 85827,
  "avatar_url": "https://gravatar.com/avatar/95a63eeb28d5e8237cfb8d6c75798ef1?d=https%3A%2F%2Fidenticons.github.com%2F2daf9eb6a6cc4da5b32440b6e6976c73.png&r=x",
  "gravatar_id": "95a63eeb28d5e8237cfb8d6c75798ef1",
  "url": "https://api.github.com/users/rodo",
  "html_url": "https://github.com/rodo",
  "followers_url": "https://api.github.com/users/rodo/followers",
  "following_url": "https://api.github.com/users/rodo/following{/other_user}",
  "gists_url": "https://api.github.com/users/rodo/gists{/gist_id}",
  "starred_url": "https://api.github.com/users/rodo/starred{/owner}{/repo}",
  "subscriptions_url": "https://api.github.com/users/rodo/subscriptions",
  "organizations_url": "https://api.github.com/users/rodo/orgs",
  "repos_url": "https://api.github.com/users/rodo/repos",
  "events_url": "https://api.github.com/users/rodo/events{/privacy}",
  "received_events_url": "https://api.github.com/users/rodo/received_events",
  "type": "User",
  "site_admin": false,
  "name": "Rodolphe Quiédeville",
  "company": "Freelance",
  "blog": "http://blog.rodolphe.quiedeville.org/",
  "location": "France",
  "email": "rodolphe@quiedeville.org",
  "hireable": false,
  "bio": "",
  "public_repos": 28,
  "public_gists": 1,
  "followers": 10,
  "following": 2,
  "created_at": "2009-05-18T12:07:44Z",
  "updated_at": "2014-01-07T12:41:20Z"
}
"""
import json
from lolyx.external.utils.LyxExternalPlugin import LyxExternalPlugin


class LyxGithub(LyxExternalPlugin):

    @classmethod
    def fetch(self, url):
        datas = {}
        (status, content) = self.download(url)
        if status == 200:
            datas = self.parse(content)
        return (status, datas)

    @classmethod
    def parse(self, data):
        """
        Return : json object
        """
        try:
            val = json.loads(data)
            public_repos = val['public_repos']
            created_at = val['created_at']
        except:
            public_repos = 0
            created_at = ''

        return {"type": "repo",
                "public_repos": public_repos,
                "created_at": created_at}

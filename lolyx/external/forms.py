# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django import forms
from django.conf import settings
from django.forms.widgets import Textarea, TextInput, Select, CheckboxInput, HiddenInput, RadioSelect
from crispy_forms.helper import FormHelper
from crispy_forms import layout
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions, InlineRadios
from lolyx.external.models import Ressource
from lolyx.external.models import UserRessource

xlarge = {'class': 'form-control'}
large = {'class': 'form-control'}
standard = {'class': 'form-control'}
date = {'class': 'datepicker'}


class UserRessourceForm(forms.ModelForm):
    """
    Use to create a new UserRessource
    """

    class Meta(object):
        model = UserRessource
        fields = ['ressource', 'userid', 'user_api']

    ressource = forms.ModelChoiceField(queryset=Ressource.objects.filter(active=True).order_by('name'),
                                       widget=Select(),
                                       label="Ressource",
                                       required=True)

    userid = forms.CharField(max_length=300,
                             required=True,
                             help_text="Identifiant sur la ressources, par exemple <b>rodo</b> dans https://gitlab.com/u/rodo",
                             label=u"Identifiant",
                             widget=TextInput())

    user_api = forms.CharField(max_length=300,
                               required=False,
                               help_text="Identifiant à utiliser pour les appels API, si différent de l'identifiant ci-dessus.",
                               label=u"Api user",
                               widget=TextInput())

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-2'
    helper.form_show_errors = True
    helper.field_class = 'col-lg-10'
    helper.layout = layout.Layout(
        layout.Field('ressource'),
        layout.Field('userid'),
        layout.Field('user_api'),
        FormActions(
            layout.Submit('save_changes', 'Enregistrer', css_class="btn-primary"),
            layout.Submit('cancel', 'Annuler', css_class="btn-danger"),
            )
        )

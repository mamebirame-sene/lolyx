# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for UserRessource object
"""
from django.contrib.auth.models import User
from django.test import TestCase
from lolyx.external.models import Ressource
from lolyx.external.models import UserRessource


class UserRessourceTests(TestCase):
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_create(self):
        """
        Create a simple ressource
        """
        ressource = Ressource.objects.create(name='Foobar')
        useres = UserRessource.objects.create(user=self.user,
                                              ressource=ressource,
                                              userid='foo')

        self.assertTrue(useres.id > 0)

    def test_userpage(self):
        """
        The userpage url
        """
        ressource = Ressource.objects.create(name='Foobar',
                                             userpage='http://foo.bar/{}')

        useres = UserRessource.objects.create(user=self.user,
                                              ressource=ressource,
                                              userid='foo')

        url = 'http://foo.bar/foo'

        self.assertEqual(useres.userpage(), url)

    def test_api_url(self):
        """
        The api url
        """
        ressource = Ressource.objects.create(name='Foobar',
                                             api_url='http://foo.bar/api/{}')

        useres = UserRessource.objects.create(user=self.user,
                                              ressource=ressource,
                                              userid='foo')

        url = 'http://foo.bar/api/foo'

        self.assertEqual(useres.api_url(), url)

    def test_api_url_special(self):
        """
        The api url with a special user api
        """
        ressource = Ressource.objects.create(name='Foobar',
                                             api_url='http://foo.bar/api/{}')

        useres = UserRessource.objects.create(user=self.user,
                                              ressource=ressource,
                                              userid='foo',
                                              user_api='32768')

        url = 'http://foo.bar/api/32768'

        self.assertEqual(useres.api_url(), url)

# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from registration.forms import RegistrationFormUniqueEmail
from registration.backends.default.views import RegistrationView
from lolyx.llx.views import (CompanyPublicListView, CompanyView,
                             HomeEmploiView, HomeStageView, HomeMissionView,
                             CompanyNewView, CompanyEditView,
                             ToolProposalNewView)
from lolyx.job.views import JobBookmarkDeleteView
from lolyx.job.views import JobPublicListView
from lolyx.resume.views import ResumeView
from lolyx.resume.views import ResumeEmploiPublicListView
from lolyx.resume.views import ResumeStagePublicListView
from lolyx.resume.views import ResumeMissionPublicListView

# Uncomment the next two lines to enable the admin:
admin.autodiscover()

urlpatterns = patterns(
    '',
    (r'^messages/', include('django_messages.urls')),
    url(r'^proxy/', include('lolyx.proxy.urls')),
    url(r'^socnet/', include('lolyx.socnet.urls')),
    url(r'^viaroute/$', 'lolyx.routing.views.viaroute'),
    url(r'^rosarks/(?P<amenity>\w+)/(?P<lon>-?\d+\.?\d{0,5})\d*/(?P<lat>-?\d+\.?\d{0,5}\d*)/$', 'lolyx.routing.views.rosarks'),
    url(r'^ressources/typeahead/(?P<prefix>\w)/$', 'lolyx.llx.views.ressources_typeahead'),
    url(r'^ressources/(?P<prefix>\w)/(?P<ressource>\w+)$', 'lolyx.llx.views.ressources'),
    url(r'^stats/main_(?P<family>\w+).json', 'lolyx.llx.views.stats_main'),
    url(r'^stats/toolmap_(?P<family>\w+).json', 'lolyx.llx.views.stats_toolmap'),
    url(r'^stats/toolmap/$', 'lolyx.stats.views.toolmap', name='stats_toolmap'),    
    url(r'^p/', include('django.contrib.flatpages.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tool/new/refuse/(?P<pk>\d+)$', 'lolyx.llx.views.refuse_tool'),
    url(r'^tool/new/accept/(?P<pk>\d+)$', 'lolyx.llx.views.accept_tool'),
    url(r'^tool/new/$', login_required(ToolProposalNewView.as_view())),
    url(r'^c/new/$', login_required(CompanyNewView.as_view()), name='company_new'),
    url(r'^c/(?P<pk>\d+)/edit/$', login_required(CompanyEditView.as_view())),
    url(r'^c/(?P<pk>\d+)/$', CompanyView.as_view()),
    url(r'^c/(?P<slug>[0-9a-zA-Z-_]+)$', CompanyView.as_view()),
    url(r'^c/$', CompanyPublicListView.as_view()),
    url(r'^cv/', include('lolyx.resume.urls')),

    # TODO need to be cleared
    url(r'^emploi/offres/', include('lolyx.job.urls')),
    url(r'^stage/offres/', include('lolyx.job.urls')),
    url(r'^mission/offres/', include('lolyx.job.urls')),
    url(r'^emploi/cv/$', ResumeEmploiPublicListView.as_view()),
    url(r'^stage/cv/$', ResumeStagePublicListView.as_view()),
    url(r'^mission/cv/$', ResumeMissionPublicListView.as_view()),
    #
    url(r'^[a-z]*/cv/(?P<pk>\d+)/$', ResumeView.as_view()),
    #
    url(r'^offres/', include('lolyx.job.urls')),
    url(r'^job/', include('lolyx.job.urls')),
    url(r'^ressources/', include('lolyx.external.urls')),
    #
    # registration
    #
    url(r'^accounts/profile/$', 'lolyx.llx.views.profile', name='profile'),
    #url(r'^accounts/register/$',
    #    RegistrationView.as_view(form_class=RegistrationFormUniqueEmail),
    #    name='registration_register'),
    #url(r'^accounts/', include('registration.backends.default.urls')),
    #url(r'^accounts/', include('django.contrib.auth.urls')),
    #
    url(r'^accounts/', include('allauth.urls')),
    #
    url(r'^emploi/$', HomeEmploiView.as_view()),
    url(r'^stage/$', HomeStageView.as_view()),
    url(r'^mission/$', HomeMissionView.as_view()),
    url(r'^$', 'lolyx.llx.views.mainhome', name='home'),
    url(r'^search/cv/$', RedirectView.as_view(url=reverse_lazy('resume'))),
    # TODO: Use reverse_lazy and not hard the path
    url(r'^search/cv/date.php$', RedirectView.as_view(url='/cv/date/')),
    url(r'^jobbookmark/(?P<pk>\d+)/delete/$', JobBookmarkDeleteView.as_view()),
    # redirect from old version
    url(r'^search/offre/offre.php$', 'lolyx.job.redirect_views.jobredirectview'),
    url(r'^search/offre/$', RedirectView.as_view(url='/emploi/offres/')),
    #
    url(r'^search/cv/cv.php$', 'lolyx.resume.redirect_views.resumeredirectview'),
)

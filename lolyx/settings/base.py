# -*- coding: utf-8 -*-
import os.path
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

SITE_ID = 1

MANAGERS = ADMINS

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': 'lolix'
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'lolyx',
#         'USER': 'rodo',
#         'PASSWORD': 'rodo',
#         'HOST': 'localhost',
#         'PORT': 5435
#     }
# }


# must be defined in local_settings.py
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
#         'NAME': '',     # Or path to database file if using sqlite3.
#         'USER': '',                      # Not used with sqlite3.
#         'PASSWORD': '',                  # Not used with sqlite3.
#         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#     }
# }

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Do not follow the default rule Debug=False to use CDN_STATIC_URL
# CDN_STATIC_URL_FORCE = False

# Tuple or string of CDN url
# CDN_STATIC_URL = ('http://a.cdn.lolix.org/', 'http://b.cdn.lolix.org/', 'http://c.cdn.lolix.org/')

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '3r%k_%oskwdl^$xnghfd3==slyt*ykqj9m^o_yvp2u2-+@*js8'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    # allauth specific context processors
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
    'django.contrib.messages.context_processors.messages',
    'lolyx.llx.context_processors.moderator',
)


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    #'lolyx.middleware.SQLLogMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'lolyx.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'lolyx.wsgi.application'

PROJECT_DIR = os.path.dirname(__file__) # this is not Django setting.

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, "..", "templates"),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

LOCALE_PATHS = (os.path.join(PROJECT_DIR, "..", "..", "locale"),)

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django_markup',
    'django_extensions',
    'endless_pagination',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'django_messages',
    'vmail',
    'djcelery',
    'compressor',
    'crispy_forms',
    'json_dbindex',
    'lolyx.llx',
    'lolyx.resume',
    'lolyx.job',
    'lolyx.external',
    'lolyx.routing',
    'lolyx.proxy',
    'lolyx.socnet',
    'lolyx.tools',
    'lolyx.stats'
)

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_MIN_LENGTH = 3
ACCOUNT_PASSWORD_MIN_LENGTH = 6
ACCOUNT_USERNAME_BLACKLIST = ['root', 'admin', 'lolix']

AUTH_PROFILE_MODULE = 'lolyx.llx.UserProfile'

# Crispy Forms
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Compressor
#
COMPRESS_CSS_FILTERS = ['compressor.filters.csstidy.CSSTidyFilter']

#COMPRESS_JS_FILTERS = ['compressor.filters.template.TemplateFilter']

#COMPRESS_TEMPLATE_FILTER_CONTEXT = {'leaflet_layer': 'http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
#                                    'leaflet_attribution': 'Map data &copy; 2014 OpenStreetMap contributors'}

COMPRESS_TEMPLATE_FILTER_CONTEXT = {'leaflet_layer': 'http://{s}.tile.quiedeville.org:81/osmfr/{z}/{x}/{y}.png',
                                    'leaflet_attribution': 'Map data © 2014 OpenStreetMap contributors'}

############################################################
# Celery
#
import djcelery
djcelery.setup_loader()
#
BROKER_URL = 'amqp://guest:guest@localhost:5672/'
#

CELERY_RESULT_SERIALIZER = 'json'
CELERY_RESULT_PERSISTENT = False

CELERY_ROUTES = {
    # emails
    'lolyx.llx.tasks.new_company_mail': {'queue': 'lolix_emails'},
    'lolyx.job.tasks.tweet_job': {'queue': 'lolix_twitter'},
}

CELERY_IGNORE_RESULT = True

#
############################################################

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },

    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
#
# Lolyx settings
#
# Points a user need to publish his offer online himself
KARMA_AUTO_MODERATE = 10
# Points added to user's karma when one of his job is accepted by a
# moderator (must be POSITIVE INTEGER VALUE)
KARMA_JOB_ACCEPTED = 1
# Points removed to user's karma when one of his job is refused by a
# moderator (must be POSITIVE INTEGER VALUE)
KARMA_JOB_REFUSED = 20
############################################################
#
# Moderation
#
############################################################
#
GROUP_MODERATOR = "Moderators"
#
# Minimal score to be set online
MDR_SCORE_MIN = 5
#
MDR_USER_POINT = 1
MDR_MODERATOR_POINT = 5
#
#############################################################
#
# How much a job cost for a company
#
POINTS_JOB_PUBLISH = 250
#
# How many points users win each month
#
POINTS_MONTHLY_ADD = 500
#
###############################################################
# User
#
# points at user creation, initial quota
USER_INITIAL_POINT = 0


FAMILY_NAMES = ('emploi', 'stage', 'mission')
FAMILY_IDS = {'emploi': 0,
             'stage': 1,
             'mission': 2}

FAMILY_EMPLOI = 0
FAMILY_STAGE = 1
FAMILY_MISSION = 2

USER_PROFILE_CANDIDAT = 1
USER_PROFILE_COMPANY = 2

###############################################################
#
# Jobs
#
# how many days a job will stay online
JOB_ONLINE_LIFETIME = 90
# how many days a resume will stay online
RESUME_ONLINE_LIFETIME = 90

# number of jobs expired by each cron run
JOB_BATCH_EXPIRE_LIMIT = 10

##############################################################################
#
# Emails
#
EMAIL_FROM = "no-reply@example.com"

##############################################################################
#
# Never change these values unless you exactly know what you do
#
JOB_STATUS_EDITION = 0
JOB_STATUS_PUBLISHED = 1
JOB_STATUS_MODERATION = 2
JOB_STATUS_REFUSED = 3
JOB_STATUS_ARCHIVED = 4
#
JOB_TEMPLATE_DEFAULT_CACHE = 300

# Cache expiration for message of the day
MOTD_TEMPLATE_DEFAULT_CACHE = 3600
#
RESUME_STATUS_EDITION = 0
RESUME_STATUS_PUBLISHED = 1
#
#
MARKUP_CHOICES=((0, 'text/plain'),
                (1, 'markdown'))

#MARKUP_CHOICES=((0, 'text/plain'),
#                (1, 'markdown'),
#                (2, 'reST'))
#
# OSRM server
#
OSRMAPI_PROTOCOL = 'http'
OSRMAPI_HOST = 'routing.rosarks.eu'
OSRMAPI_PORT = '5000'

##############################################################################
#
# Rosarks geo provider

ROSARKS_PROTOCOL = 'http'
ROSARKS_HOST = 'rosarks.eu'
ROSARKS_PORT = '80'

# timeout in seconds
ROSARKS_TIMEOUT = 10

# how long the view is cached
ROSARKS_TIMECACHE = 10

# how long the view is cached
LLXIO_TIMECACHE = 300

#
# Mails Alias
#
VMAIL_DOMAIN_ID = 1
VMAIL_ALIAS_DOMAIN = 'lolixmail.org'

##############################################################################
# Twitter

TWITTER_CONSUMER_KEY = ''
TWITTER_CONSUMER_SECRET = ''
TWITTER_ACCESS_TOKEN_KEY = ''
TWITTER_ACCESS_TOKEN_SECRET = ''

# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.http import HttpResponse
from lolyx.routing.utils import readjson


logger = logging.getLogger(__name__)


@cache_page(settings.ROSARKS_TIMECACHE)
def viaroute(request):
    """
    Call OSRM
    """
    cfrom = request.GET['from']
    cto = request.GET['to']

    url = "{}://{}:{}/viaroute?loc={}&loc={}&instructions=false"

    data = readjson(url.format(settings.OSRMAPI_PROTOCOL,
                               settings.OSRMAPI_HOST,
                               settings.OSRMAPI_PORT,
                               cfrom,
                               cto))

    return HttpResponse(data, mimetype='application/json')


@cache_page(settings.ROSARKS_TIMECACHE)
def rosarks(request, amenity, lon, lat):
    """
    Call rosarks
    """
    try:
        precision = request.GET['precision']
    except:
        precision = ""

    try:
        limit = request.GET['limit']
    except:
        limit= ""

    url = "{}://{}:{}/{}/{}/{}/?precision={}&limit={}";

    data = readjson(url.format(settings.ROSARKS_PROTOCOL,
                               settings.ROSARKS_HOST,
                               settings.ROSARKS_PORT,
                               amenity,
                               lon,
                               lat,
                               precision,
                               limit))

    return HttpResponse(data,
                        mimetype='application/json')

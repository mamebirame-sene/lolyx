#
#
#
from django import template
from django.conf import settings

register = template.Library()

# settings value
@register.filter
def family_name(value):
    return settings.FAMILY_NAMES[value].upper()

@register.filter
def family_initiale(value):
    return (settings.FAMILY_NAMES[value][0:1]).upper()

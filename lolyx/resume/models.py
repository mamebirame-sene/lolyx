# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models definition for resume
"""
from django.conf import settings
from datetime import datetime, timedelta
from django_pg import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from lolyx.llx.models import Company
from lolyx.llx.models import Lang
from lolyx.llx.models import Region
from lolyx.llx.models import Tool
from lolyx.llx.models import UserProfile
from lolyx.socnet.models import Network
from lolyx.utils.strgen import resume_email_random


class Resume(models.Model):
    """
    A resume
    """
    user = models.ForeignKey(User,
                             on_delete=models.PROTECT)
    title = models.CharField(max_length=300,
                             verbose_name='Title',
                             blank=True,
                             null=True)

    name = models.CharField(max_length=50)
    firstname = models.CharField(max_length=50)

    # quel type de recherche
    family = models.IntegerField(default=0,
                                 choices=((settings.FAMILY_EMPLOI, 'Emploi'),
                                          (settings.FAMILY_STAGE, 'Stage'),
                                          (settings.FAMILY_MISSION, 'Mission')))

    poste_desc = models.TextField(blank=True)

    poste_desc_markup = models.PositiveSmallIntegerField(
        default=0,
        choices=settings.MARKUP_CHOICES)

    # inactive : the resume is not online
    # active : the resume is published online
    status = models.PositiveSmallIntegerField(
        default=0,
        choices=((0, 'inactive'),
                 (1, 'active')))

    viewed = models.IntegerField(default=0)

    email = models.EmailField(max_length=300)
    email_verified = models.BooleanField(default=False)

    # internal generated 
    email_internal = models.EmailField(max_length=300,
                                       unique=True)

    epoch = datetime(1970, 1, 1, 0, 0, 0, 0)
    date_published = models.DateTimeField(auto_now_add=False,
                                          default=epoch)
    date_updated = models.DateTimeField(auto_now=True)
    # When the job will be unpublished, or when the job was unpublished
    date_expired = models.DateTimeField(auto_now_add=False,
                                        default=epoch)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    @property
    def online(self):
        return self.status == 1

    def get_absolute_url(self):
        return '/cv/{}/'.format(self.id)

    def publish(self):
        """The resume will be displayed online
        """
        if self.status == settings.RESUME_STATUS_EDITION:
            self.dopublish()

    def dopublish(self):
        self.status = settings.RESUME_STATUS_PUBLISHED
        self.date_published = datetime.now()
        self.date_expired = self.date_published + timedelta(days=settings.RESUME_ONLINE_LIFETIME)
        self.save()

        tools = ResumeTool.objects.filter(resume_id=self.id).values_list('tool__slug')
        tool_list = [tool[0] for tool in tools]
        ResumeOnline.objects.create(resume=self,
                                    tools=tool_list)
        
        ResumeLog.objects.get_or_create(resume=self,
                                        status=self.status)
        # job moderauth
        uprof = UserProfile.objects.get(user=self.user)
        uprof.give_jobmoder()

    def unpublish(self):
        """The resume will no longer be displayed online
        """
        if self.status == settings.RESUME_STATUS_PUBLISHED:
            self.status = settings.RESUME_STATUS_EDITION
            self.date_expired = datetime.now()
            self.save()
            ResumeOnline.objects.filter(resume=self).delete()
            ResumeLog.objects.get_or_create(resume=self,
                                            status=self.status)


class ResumeOnline(models.Model):
    """Lists resume actually online

    With a finite list of resume online associated with a sequence,
    it is possible to do key pagination
    """
    resume = models.ForeignKey(Resume)
    tools = models.ArrayField(models.CharField(max_length=30))

class ResumeExperience(models.Model):
    """An experience
    """
    resume = models.ForeignKey(Resume)

    date_deb = models.DateField(auto_now_add=False)
    date_fin = models.DateField(auto_now_add=False)

    title = models.CharField(max_length=100)

    desc = models.TextField(blank=True)

    desc_markup = models.PositiveSmallIntegerField(
        default=0,
        choices=settings.MARKUP_CHOICES)


class ResumeRegion(models.Model):
    """Region with a preference order
    """
    resume = models.ForeignKey(Resume)
    region = models.ForeignKey(Region)
    pref = models.PositiveSmallIntegerField(default=0)


class ResumeTool(models.Model):
    """Store extra information per user
    """
    resume = models.ForeignKey(Resume)
    tool = models.ForeignKey(Tool,
                             on_delete=models.PROTECT)
    level = models.PositiveSmallIntegerField(default=3)
    contrib = models.PositiveSmallIntegerField(default=0)
    # in years
    experience = models.PositiveSmallIntegerField(default=0)

    class Meta:
        unique_together = ('resume', 'tool',)


class ResumeStat(models.Model):
    """Store when and who view a resume
    """
    resume = models.ForeignKey(Resume)
    datetime = models.DateTimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)


class ResumeLog(models.Model):
    """Log action on resume's status
    """
    resume = models.ForeignKey(Resume)
    date = models.DateTimeField(auto_now_add=True)
    status = models.PositiveSmallIntegerField(default=0,
                                              choices=((0, 'inactive'),
                                                       (1, 'active')))


class CompanyFollowed(models.Model):
    """The company followed

    Will receive an email when the company post a new job
    """
    user = models.ForeignKey(User)
    company = models.ForeignKey(Company)
    datetime = models.DateTimeField(auto_now_add=True)


class ResumeLang(models.Model):
    """Store required langs knowledge
    """
    resume = models.ForeignKey(Resume)
    lang = models.ForeignKey(Lang)
    level = models.SmallIntegerField(default=3)

    # usefull for statistics
    tms = models.DateTimeField(auto_now_add=True,
                               default=datetime.now())

    class Meta:
        unique_together = ('resume', 'lang',)


def create_resume_init(sender, instance, created, **kwargs):
    """Actions on resume creation
    """
    if created:
        ResumeLog.objects.get_or_create(resume=instance)
        # email internal must be unique
        emailr = resume_email_random()
        while (Resume.objects.filter(email_internal=emailr).exists()):
            emailr = resume_email_random()
        
        instance.email_internal = emailr

post_save.connect(create_resume_init, sender=Resume)

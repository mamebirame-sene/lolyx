# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django import forms
from django.conf import settings
from django.forms.widgets import Textarea, TextInput, Select, CheckboxInput, HiddenInput, RadioSelect
from lolyx.resume.models import Resume, ResumeExperience

from lolyx.llx.models import UserProfile
from crispy_forms.helper import FormHelper
from crispy_forms import layout
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions, InlineRadios

xlarge = {'class': 'form-control'}
large = {'class': 'form-control'}
standard = {'class': 'form-control'}
date = {'class': 'datepicker'}


class ExperienceForm(forms.ModelForm):
    """
    Use to create Experience
    """
    class Meta(object):
        model = ResumeExperience
        # user will be set in views.ResumeNew
        # other fields will be set as model default
        exclude = ('resume',)

    resumid = forms.CharField(widget=HiddenInput())

    date_deb = forms.CharField(max_length=10,
                               required=True,
                               label=u"Date de début",
                               widget=TextInput(attrs=date))

    date_fin = forms.CharField(max_length=10,
                               required=True,
                               label=u"Date de fin",
                               widget=TextInput(attrs=date))

    title = forms.CharField(max_length=100,
                            required=True,
                            label=u"Titre",
                            widget=TextInput(attrs=xlarge))

    desc = forms.CharField(max_length=2500,
                           required=False,
                           label=u"Poste",
                           widget=Textarea())

    desc_markup = forms.ChoiceField(required=True,
                                    label="Format",
                                    choices=((0, 'text/plain'),
                                             (1, 'markdown'),
                                             (2, 'reST')),
                                    initial='1')

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-2'
    helper.field_class = 'col-lg-10'
    helper.layout = layout.Layout(
        'resumid',
        layout.Field('date_deb'),
        layout.Field('date_fin'),
        layout.Field('title'),
        layout.Field('desc'),
        InlineRadios('desc_markup'),
        FormActions(
            layout.Submit('save_changes', 'Enregistrer', css_class="btn-primary"),
            layout.Submit('cancel', 'Annuler', css_class="btn-danger"),
            )
        )



class ResumeForm(forms.ModelForm):
    """Use to create and edit Resume
    """
    class Meta(object):
        model = Resume
        # user will be set in views.ResumeNew
        # other fields will be set as model default
        exclude = ('status', 'date_published', 'date_expired',
                   'viewed', 'user')


    family = forms.ChoiceField(required=True,
                               label="Type",
                               choices=((settings.FAMILY_EMPLOI, 'Emploi'),
                                        (settings.FAMILY_STAGE, 'Stage'),
                                        (settings.FAMILY_MISSION, 'Mission')),
                               widget = RadioSelect(),
                               initial = '0',
                               )

    status = forms.ChoiceField(required=True,
                               label="Recherche",
                               choices=((0, 'active'),
                                        (1, 'passive')),
                               widget = RadioSelect(),
                               initial = '0',
                               )

    InlineRadios('family'),

    name = forms.CharField(max_length=50,
                           required=True,
                           label="Nom",
                           widget=TextInput())

    firstname = forms.CharField(max_length=50,
                                required=True,
                                label=u"Prénom",
                                widget=TextInput())

    email = forms.CharField(max_length=50,
                            required=True,
                            widget=TextInput())

    title = forms.CharField(max_length=50,
                            required=True,
                            label='Titre',
                            widget=TextInput())

    poste_desc = forms.CharField(max_length=2500,
                                 required=False,
                                 label="Poste",
                                 widget=Textarea())

    poste_desc_markup = forms.ChoiceField(required=True,
                                          label="Format",
                                          choices=((0, 'text/plain'),
                                                   (1, 'markdown'),
                                                   (2, 'reST')),
                                          initial='1')

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-2'
    helper.field_class = 'col-lg-10'
    helper.layout = layout.Layout(
        InlineRadios('family'),
        layout.Field('title', css_class='input-large'),
        InlineRadios('status'),
        layout.Field('poste_desc'),
        InlineRadios('poste_desc_markup'),
        PrependedText('email', '@', css_class='input-xlarge'),

        layout.Fieldset(
            u'Compétences',
            layout.Div(template='toolahead.html')),

        layout.Fieldset(u'Coordonnées',
                        layout.Field('name'),
                        layout.Field('firstname')),

        FormActions(
            layout.Submit('save_changes', 'Enregistrer', css_class="btn-primary"),
            layout.Submit('cancel', 'Annuler', css_class="btn-danger"),
            )
        )


class PreferenceForm(forms.ModelForm):
    """User preferences
    """
    class Meta(object):
        model = UserProfile
        # user will be set in views.ResumeNew
        # other fields will be set as model default
        exclude = ('user', 'karma', 'status','monthly_points',
                   'jobmoder_allowed',
                   'jobmoder_blacklist','jobmoder_point')


    lon = forms.FloatField(required=True,
                           label="Longitude",
                           widget=TextInput(attrs=xlarge))

    lat = forms.FloatField(required=True,
                           label="Latitude",
                           widget=TextInput(attrs=xlarge))


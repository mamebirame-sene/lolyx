/*
 * Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
var marker = null;

function addmap(lon, lat) {

    var div = "prefmap";
    var center = [46.0, 2.0];
    var zoom = 5;

    if (lon + lat != 0.0) {
	center = [lat, lon];
	zoom = 12;
    }

    var map = L.map(div).setView(center, zoom);
    var osmfr = L.tileLayer(map_main_layers[0],
			    {attribution: map_main_layers[1]}).addTo(map);

    map.on('click', onMapClick);

    if (lon + lat != 0.0) { addMarker(lon, lat); }

    function onMapClick(e) {
	if (marker) {
	    map.removeLayer(marker);
	}

	marker = L.marker(e.latlng, {icon: defaultIcon});
	marker.addTo(map);
	updateForm(e);
    };

    function addMarker(lon, lat) {
	if (lon + lat != 0) {
	    marker = L.marker([lat, lon], {icon: defaultIcon});
	    marker.addTo(map);
	}
    };
}

function updateForm(e) {
    // Update the values in input
    $("#id_lon").val(e.latlng.lng);
    $("#id_lat").val(e.latlng.lat);
}


# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013,2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.conf.urls import patterns, url
from lolyx.resume.views import (ResumeView, ResumeEdit,
                                ResumePublicListView, ResumeNew,
                                ResumeEditTools,
                                ExperienceNew, PreferenceView, ExternalView)

urlpatterns = patterns('',

                       url(r'^(?P<pk>\d+)/experience/new/$', ExperienceNew.as_view()),
                       url(r'^(?P<pk>\d+)/stats/views/$', 'lolyx.resume.views.stats_views'),
                       url(r'^(?P<pk>\d+)/unpublish/$', 'lolyx.resume.views.unpublish'),
                       url(r'^(?P<pk>\d+)/publish/$', 'lolyx.resume.views.publish'),

                       url(r'^(?P<pk>\d+)/edit/addtool/(?P<toolid>\d+)$', 'lolyx.resume.views.addtool'),
                       url(r'^(?P<pk>\d+)/edit/tools/addtool/(?P<toolid>\d+)$', 'lolyx.resume.views.addtool'),
                       url(r'^(?P<pk>\d+)/edit/tools/deltool/(?P<toolid>\d+)$', 'lolyx.resume.views.deltool'),
                       url(r'^(?P<pk>\d+)/edit/$', ResumeEdit.as_view()),
                       url(r'^(?P<pk>\d+)/edit/tools/$', ResumeEditTools.as_view()),
                       url(r'^(?P<pk>\d+)/$', ResumeView.as_view(), name='resume'),
                       url(r'^new/$', ResumeNew.as_view()),
                       url(r'^pref/(?P<pk>\d+)$', PreferenceView.as_view()),
                       url(r'^external/(?P<pk>\d+)/remove/$', 'lolyx.resume.views.external_delete', name='resume_external_remove'),
                       url(r'^external/$', ExternalView.as_view(), name='resume_external'),
                       url(r'^$', ResumePublicListView.as_view())
                       )

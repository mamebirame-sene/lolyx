# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The Lolyx llx views
"""
import logging
import json
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic import (DetailView, ListView, CreateView,
                                  UpdateView, TemplateView)
from lolyx.job.models import Job, JobVote, JobBookmark, JobUserView
from lolyx.llx.models import (Tool, Motd, UserProfile, UserPoint,
                              Company, Sector, Poste, Lang, ToolProposal)
from lolyx.resume.models import Resume
from lolyx.resume.models import ResumeOnline
from lolyx.llx.tasks import toolproposal_accepted, toolproposal_refused
from lolyx.llx.forms import CompanyNewForm, ToolProposalForm
from django.shortcuts import get_object_or_404

logger = logging.getLogger(__name__)


class ToolProposalNewView(CreateView):
    """Create a new toolProposal
    """
    model = ToolProposal
    template_name = 'llx/toolproposal_new.html'
    form_class = ToolProposalForm
    success_url = '/tool/new/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(ToolProposalNewView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ToolProposalNewView, self).get_context_data(**kwargs)
        context['tools'] = ToolProposal.objects.all()
        return context


class CompanyPublicListView(ListView):
    model = Company
    template_name = 'llx/company_public_list.html'

    def get_queryset(self):
        return Company.objects.all()


class CompanyNewView(CreateView):
    """Create a new company
    """
    model = Company
    template_name = 'llx/company_new.html'
    form_class = CompanyNewForm
    success_url = '/accounts/profile/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CompanyNewView, self).form_valid(form)


class CompanyEditView(UpdateView):
    """Edit an existing company
    """
    model = Company
    template_name = 'llx/company_edit.html'
    form_class = CompanyNewForm
    success_url = '/accounts/profile/'

    def get_object(self, *args, **kwargs):
        return get_object_or_404(Company, pk=self.kwargs.get('pk'),
                                 user__id=self.request.user.id)


class CompanyView(DetailView):
    """The public view
    Anonymous mode
    """
    model = Company

    def get_context_data(self, **kwargs):
        context = super(CompanyView, self).get_context_data(**kwargs)
        jobs = Job.objects.filter(
            company=self.object,
            status=settings.JOB_STATUS_PUBLISHED).order_by('-date_published')[:5]

        context['jobs'] = jobs
        return context

    def get(self, *args, **kwargs):
        # If a company has no offer online we don't display it
        self.object = self.get_object()
        context = self.get_context_data()
        if len(context['jobs']) == 0 and self.object.stay_alive is False:
            return redirect('/')
        return super(CompanyView, self).get(*args, **kwargs)


class HomeView(TemplateView):
    """
    The home page
    """
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        jobs = Job.objects.filter(
            status=settings.JOB_STATUS_PUBLISHED,
            family=self.family).select_related('company').order_by('-date_published')[:5]

        resumes = ResumeOnline.objects.filter(resume__family=self.family).values_list('resume__id',
                                                                                      'resume__title').order_by('-pk')[:5]

        motds = Motd.objects.filter(active=True, home=True)
        motds = motds.order_by('-date_created')[:2]

        labels = ['emploi', 'stage', 'mission']

        context['family'] = self.family
        context['familyname'] = labels[self.family]
        context['resumes'] = resumes
        context['jobs'] = jobs
        context['motds'] = motds
        return context


class HomeEmploiView(HomeView):
    template_name = 'llx/home_emploi.html'
    family = 0


class HomeStageView(HomeView):
    template_name = 'llx/home_stage.html'
    family = 1


class HomeMissionView(HomeView):
    template_name = 'llx/home_mission.html'
    family = 2


@login_required
def profile(request):
    """The profile wiew

    This view is shared betweeb job and resume, not the best idea I ever add.
    """
    userprof = UserProfile.objects.get(user=request.user)
    bookmarks = JobBookmark.objects.filter(user=request.user).select_related('job__company').order_by('-date')[:5]

    # Update the profile as the first login
    if userprof.status == 0:
        status = 0
        try:
            status = int(request.GET['set'])
        except:
            pass

        if status == 1 or status == 2:
            userprof.status = status
            userprof.save()
        else:
            result = render(request, 'llx/profile.html', {})

    # candidat is logged
    if userprof.status == settings.USER_PROFILE_CANDIDAT:
        resumes = Resume.objects.filter(user=request.user).order_by('-date_updated')

        my_votes = JobVote.objects.filter(user=request.user).order_by('-date')[:7]
        jobsviewed = JobUserView.objects.filter(user=request.user).select_related('job__company').order_by('-datetime')[:5]
        result = render(request,
                        'resume/profile.html',
                        {'resumes': resumes,
                         'my_votes': my_votes,
                         'jobsviewed': jobsviewed,
                         'bookmarks': bookmarks,
                         'profile': userprof})
    # company
    if userprof.status == settings.USER_PROFILE_COMPANY:
        userpoint = UserPoint.objects.get(user=request.user)
        companies = Company.objects.filter(user=request.user, is_deleted=False)
        jobs_e = Job.objects.filter(company__user=request.user,
                                    status=settings.JOB_STATUS_EDITION)
        jobs_o = Job.objects.filter(company__user=request.user,
                                    status=settings.JOB_STATUS_PUBLISHED)
        jobs_r = Job.objects.filter(company__user=request.user,
                                    status=settings.JOB_STATUS_REFUSED)
        jobs_m = Job.objects.filter(company__user=request.user,
                                    status=settings.JOB_STATUS_MODERATION)
        jobs_a = Job.objects.filter(company__user=request.user,
                                    status=settings.JOB_STATUS_ARCHIVED)
        result = render(request,
                        'job/profile.html',
                        {'user': request.user,
                         'bookmarks': bookmarks,
                         'companies': companies,
                         'jobs_edit': jobs_e,
                         'jobs_online': jobs_o,
                         'jobs_refused': jobs_r,
                         'jobs_moderation': jobs_m,
                         'jobs_archived': jobs_a,
                         'profile': userprof,
                         'points': userpoint})
    return result


def mainhome(request):
    """
    The home page
    """
    jobs = Job.objects.filter(
        status=settings.JOB_STATUS_PUBLISHED,
        family=settings.FAMILY_EMPLOI).order_by('-date_published')[:5]

    stages = Job.objects.filter(
        status=settings.JOB_STATUS_PUBLISHED,
        family=settings.FAMILY_STAGE).order_by('-date_published')[:5]

    missions = Job.objects.filter(
        status=settings.JOB_STATUS_PUBLISHED,
        family=settings.FAMILY_MISSION).order_by('-date_published')[:5]

    resumes = Resume.objects.filter(
        status=settings.RESUME_STATUS_PUBLISHED).order_by('-date_published')[:5]

    motds = Motd.objects.filter(active=True,
                                home=True).order_by('-date_published')[:1]

    return render(request,
                  'home.html',
                  {'resumes': resumes,
                   'jobs': jobs,
                   'missions': missions,
                   'stages': stages,
                   'motds': motds})


def stats_main(request, family):
    """Return the main stats

    All stats views must be built on cached values
    """
    datas = [{'label': 'offre', 'value': cache.get('job_{}_nb_online'.format(family))},
             {'label': 'cv', 'value': cache.get('resume_{}_nb_online'.format(family))}]

    return HttpResponse(json.dumps(datas),
                        mimetype='application/json')


@cache_page(3600)
def ressources(request, prefix, ressource):
    """Ressources

    Return : json
    """
    datas = []
    if (prefix == 't'):
        objects = Tool.objects.filter(name__startswith=ressource).order_by('name')

    if (prefix == 's'):
        objects = Sector.objects.filter(name__startswith=ressource).order_by('name')

    if (prefix == 'p'):
        objects = Poste.objects.filter(name__startswith=ressource).order_by('name')

    for obj in objects:
        datas.append({'id': obj.id, 'name': obj.name, 'slug': obj.slug})

    return HttpResponse(json.dumps({"result": 0,
                                    "datas": datas}),
                        mimetype='application/json')


@cache_page(3600)
def ressources_typeahead(request, prefix):
    """Ressources

    Return : json
    """
    datas = []
    if (prefix == 't'):
        objects = Tool.objects.all().order_by('name')

    if (prefix == 's'):
        objects = Sector.objects.all().order_by('name')

    if (prefix == 'p'):
        objects = Poste.objects.all().order_by('name')

    if (prefix == 'l'):
        objects = Lang.objects.all().order_by('name')

    for obj in objects:
        datas.append({'id': obj.id, 'label': obj.name, 'slug': obj.slug})

    return HttpResponse(json.dumps(datas),
                        mimetype='application/json')


@login_required
def accept_tool(request, pk):
    """Accept a tool proposal

    Create a new Tool, Delete the ToolProposal
    """
    if request.user.is_staff:
        try:
            tlp = ToolProposal.objects.get(pk=pk)
        except ObjectDoesNotExist:
            tlp = None

        if tlp is not None:
            # Create a new tool
            Tool.objects.create(name=tlp.name,
                                slug=tlp.slug,
                                url=tlp.url)
            # Send a mail to whom propose the tool
            toolproposal_accepted.delay(tlp, request.user)
            # Delete the proposal
            tlp.delete()

    return redirect('/tool/new/')


@login_required
def refuse_tool(request, pk):
    """Refuse a tool proposal

    Create a new Tool, Delete the ToolProposal
    """
    if request.user.is_staff:
        try:
            tlp = ToolProposal.objects.get(pk=pk)
        except ObjectDoesNotExist:
            tlp = None

        if tlp is not None:
            # Send a mail to whom propose the tool
            toolproposal_refused.delay(tlp, request.user)
            # Delete the proposal
            tlp.delete()

    return redirect('/tool/new/')


def stats_toolmap(request, family):
    """Return the main stats

    All stats views must be built on cached values
    """
    return HttpResponse(cache.get('tool_map_{}'.format(family)),
                        mimetype='application/json')

# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test twitter access
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager
from datetime import datetime

class Command(BaseCommand):
    help = 'Test twitter access'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        api = TwitterAPI(settings.TWITTER_CONSUMER_KEY,
                         settings.TWITTER_CONSUMER_SECRET,
                         settings.TWITTER_ACCESS_TOKEN_KEY,
                         settings.TWITTER_ACCESS_TOKEN_SECRET)

        r = api.request('account/verify_credentials')
        print(r.text)
        r = api.request('statuses/update',
                        {'status': 'the time is now %s' % datetime.now()})
        print(r.status_code)

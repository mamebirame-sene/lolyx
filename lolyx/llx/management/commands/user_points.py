#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output stats for munin
"""
from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from lolyx.llx.models import UserPoint
from lolyx.llx.models import UserProfile


class Command(BaseCommand):
    help = 'Print tools on stdout'

    def handle(self, *args, **options):
        """
        Update the number of points of all users
        """
        delta = 30
        dtn = datetime.now() - timedelta(days=delta)

        userps = UserPoint.objects.filter(date_updated__lt=dtn,
                                          active=True)

        for userp in userps:
            uprof = UserProfile.objects.get(user=userp.user)

            if userp.points < uprof.monthly_points:
                userp.points = uprof.monthly_points
                userp.save()
            self.stdout.write('{} {} {}\n'.format(userp.id,
                                                  userp.user.username,
                                                  uprof.monthly_points))

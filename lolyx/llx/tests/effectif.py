# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Effectif object
"""
from django.test import TestCase
from lolyx.llx.models import Effectif


class EffectifTests(TestCase):
    """
    The main tests
    """

    def test_create(self):
        """
        Create a simple effectif
        """
        effectif = Effectif.objects.create(name='Tout petit')

        self.assertTrue(effectif.id > 0)
        self.assertEqual(str(effectif), 'Tout petit')
        self.assertEqual(unicode(effectif), 'Tout petit')

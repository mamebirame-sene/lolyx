# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test forms in app llx
"""
from django.test import TestCase
from lolyx.llx import models
from lolyx.llx import forms


class FormsTests(TestCase):
    """
    Tests regarding the object Forms
    """
    def setUp(self):
        """Set up the tests
        """
        models.Effectif.objects.all().delete()
        models.Sector.objects.all().delete()
        models.Typent.objects.all().delete()

    def test_companynewform(self):
        """Form used to create a new Company

        - the form is prefilled with some values
        """
        form = forms.CompanyNewForm()

        # pk=0 will not be loaded
        models.Effectif.objects.create(pk=0, name='Moins petit')
        models.Effectif.objects.create(pk=1, name='Plus petit')
        models.Effectif.objects.create(pk=2, name='Moyen petit')

        # pk=0 will not be loaded
        models.Sector.objects.create(pk=0, name='lorem', slug='lorem')
        models.Sector.objects.create(pk=1, name='ipsum', slug='ipsum')

        # pk=0 will not be loaded
        models.Typent.objects.create(pk=0, name='lorem')
        models.Typent.objects.create(pk=1, name='ipsum')

        self.assertEqual(len(form.fields['effectif'].choices), 2)
        self.assertEqual(len(form.fields['sector'].choices), 1)
        self.assertEqual(len(form.fields['typent'].choices), 1)

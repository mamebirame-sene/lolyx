# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Company object
"""
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.test import TestCase
from lolyx.llx.models import Company


class CompanyTests(TestCase):
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_create(self):
        """
        Create a simple company
        """
        company = Company.objects.create(user=self.user, name='Lolix')

        self.assertTrue(company.id > 0)
        self.assertFalse(company.adh_april)
        self.assertFalse(company.ssii)
        self.assertFalse(company.cabrecrut)
        self.assertEqual(str(company), 'Lolix')
        self.assertEqual(unicode(company), 'Lolix')

    def test_create_slugunicity(self):
        """
        Create a simple company
        """
        Company.objects.create(user=self.user,
                               name='Lolix',
                               slug='wuCirae4')

        with self.assertRaises(IntegrityError):
            Company.objects.create(name='Foobar',
                                   slug='wuCirae4')

    def test_absolute_url(self):
        """
        Absolute url
        """
        comp = Company.objects.create(user=self.user,
                                      name='Lolix')

        url = "/c/{}/".format(comp.id)

        self.assertEqual(comp.get_absolute_url(), url)

    def test_absolute_url_numeral_name(self):
        """
        Absolute url
        """
        comp = Company.objects.create(user=self.user, name='42')

        url = "/c/{}/".format(comp.id)

        self.assertEqual(comp.get_absolute_url(), url)

    def test_absolute_url_with_slug(self):
        """
        Absolute url
        """
        comp = Company.objects.create(user=self.user, name='Lolix',
                                      slug='wuCirae4')

        url = "/c/{}".format(comp.slug)

        self.assertEqual(comp.get_absolute_url(), url)

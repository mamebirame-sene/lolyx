/*
 * Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
var geojson;
var families = ["emploi", "stage", "mission"];
var stats = [];

function maps() {
    mapi(0);
    mapi(1);
    mapi(2);
}

function mapi(i) {

    var div = families[i] + "map";

    var map = L.map(div).setView([46.0, 2.0], 5);
    var osmfr = L.tileLayer(map_main_layers[0], {attribution: map_main_layers[1]}).addTo(map);
    
    var layer = L.geoJson();
    var markers = new L.MarkerClusterGroup();

    var pophtml = '<div><a href="{{url}}">{{title}}</a></div><div>{{company}}</div>';
    
    var url = "/offres/stats/"+families[i]+"/coord.json";
    $.getJSON(url,
	      function( datas ) {
		      $.each(datas, function (data) {

                  var marker = L.marker(new L.LatLng(this.lat, this.lon), { title: this.title });
                  var popup = Mustache.render(pophtml, {'title': this.title,
                                                        'company': this.company,
                                                        'url': this.url});
                  marker.bindPopup(popup);
                  markers.addLayer(marker);

              });
          });

    map.addLayer(markers);
}




function getNbOffre(nom, family) {
    return stats[family][nom];
}

function style_emploi(feature) {

    var d = getNbOffre(feature.properties.code_insee, 0);
    var color = getColor(feature.properties.code_insee, 0);

    var style = { weight: 1,
		  opacity: 1,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0.8,
		  fillColor: color
		};

    if (d ==0) {
	style = { weight: 1,
		  opacity: 0,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0
		};
    }

    return style;
}

function style_stage(feature) {
    var d = getNbOffre(feature.properties.code_insee, 1);
    var color = getColor(feature.properties.code_insee, 1);

    var style = { weight: 1,
		  opacity: 1,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0.8,
		  fillColor: color
		};

    if (d ==0) {
	style = { weight: 2,
		  opacity: 0,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0
		};
    }

    return style;
}

function style_mission(feature) {
    var d = getNbOffre(feature.properties.code_insee, 2);
    var color = getColor(feature.properties.code_insee, 2);

    var style = { weight: 1,
		  opacity: 1,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0.8,
		  fillColor: color
		};

    if (d ==0) {
	style = { weight: 1,
		  opacity: 0,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0
		};
    }

    return style;
}

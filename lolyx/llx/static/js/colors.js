/*
 * 
 * 
 * 
 * 
 */
function colors(family) {
    var colors = [colorbrewer.Blues[3], 
		  colorbrewer.Greens[3], 
		  colorbrewer.Oranges[3]
		 ];
    return colors[family];
}

function mapcolors(family) {
    var colors = [colorbrewer.Blues[7], 
		  colorbrewer.Greens[7], 
		  colorbrewer.Oranges[7]
		 ];
    return colors[family];
}



$(document).ready(
    function() {
	//$("#div_id_slug").addClass('has-success has-feedback');
	//$("#div_id_slug > div").append('<span id="glyphahead" class="glyphicon glyphicon-ok form-control-feedback"></span>');
	//$("#glyphahead").insertBefore( $("#hint_id_slug") );

	function parset(data, val) {
	    var slugs = [];
	    var exist = false;
            $.each(data,
		   function(i, object) {
		       slugs.push('<span>'+object.slug+'</span>');
		       exist = (object.slug == val);
		   });
	    $("#slug_exists").append(slugs.join(', '));

	    document.getElementById('submit-id-save_changes').disabled = exist;
	};

	function parsename(data, val) {
	    var names = [];
	    var exist = false;
            $.each(data,
		   function(i, object) {
		       names.push('<span>'+object.name+'</span>');
		       exist = (object.name.toLowerCase() == val.toLowerCase());
		   });
	    $("#name_exists").append(names.join(', '));

	    document.getElementById('submit-id-save_changes').disabled = exist;
	};


	function lookuptool(val, field) {
	    if (field == 'name') {
		$.get('/ressources/t/'+val,
		      function(data) {
			  parsename(data.datas, val);
		      });
	    } else {
		$.get('/ressources/t/'+val,
		      function(data) {
			  parset(data.datas, val);
		      });
	    }
	};

	function validate(value) {
	    var result = true;
	    var valid = value.replace(/[^a-z0-9]/gi,'');
	    if (valid.length == 0 || valid.length > 15) {
		result = false;
	    }
	    if (value != valid) {
		result = false;
	    }
	    return result;
	}

	/* Typehead initialisation
	 *
	 * We look for existing value on each keypress
	 *
	 */
	$("#id_slug").keyup(
	    function() {
		$("#slug_exists").empty();
		var val = $("#id_slug").val();
		if (validate(val)) {
		    lookuptool(valid, 'slug');
		} else {
		    document.getElementById('submit-id-save_changes').disabled = true;
		}
	    });

	$("#id_name").keyup(
	    function() {
		$("#name_exists").empty();
		var val = $("#id_name").val();
		if (validate(val)) {
		    lookuptool(val, 'name');
		} else {
		    document.getElementById('submit-id-save_changes').disabled = true;
		}
	    });
    }
);


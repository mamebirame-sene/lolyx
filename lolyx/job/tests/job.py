# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Job object

"""
from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from django.db import IntegrityError
from lolyx.llx.models import Company
from lolyx.llx.models import UserProfile
from lolyx.llx.models import Tool
from lolyx.job.models import Job
from lolyx.job.models import JobTool


class JobTests(TestCase):
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

        self.company = Company.objects.create(user=self.user, name='Foo corp')

    def test_create(self):
        """
        Create a simple job
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        self.assertTrue(job.id > 0)
        self.assertEqual(job.moderation_votes, 0)
        self.assertEqual(job.moderation_score, 0)

    def test_create_tool(self):
        """
        Create a job with a tool
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        tool = Tool.objects.create(name='foo', slug='foo')

        restool = JobTool.objects.create(job=job,
                                         tool=tool)

        self.assertTrue(restool.id > 0)

    def test_create_tools(self):
        """
        Create a job with more than one tool
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        tool1 = Tool.objects.create(name='foo1', slug='tool1')
        tool2 = Tool.objects.create(name='foo2', slug='tool2')

        restool = JobTool.objects.create(job=job,
                                         tool=tool1)

        restool = JobTool.objects.create(job=job,
                                         tool=tool2)

        self.assertTrue(restool.id > 0)

    def test_create_tool_unique(self):
        """
        Check unicity of job/tool couple
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        tool1 = Tool.objects.create(name='foo1', slug='fool1')

        JobTool.objects.create(job=job, tool=tool1)

        with self.assertRaises(IntegrityError):
            JobTool.objects.create(job=job, tool=tool1)

    def test_refuse(self):
        """
        Refuse a job

        When a job is refused the users's karma is updated
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')
        authprof = UserProfile.objects.get(user=author)
        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        job.refuse(-settings.MDR_SCORE_MIN)
        after_prof = UserProfile.objects.get(user=author)

        self.assertTrue(after_prof.karma < authprof.karma)

    def test_publish_moderation(self):
        """Publish a job

        When a job is published the users's karma is updated
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')
        authprof = UserProfile.objects.get(user=author)

        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_MODERATION)

        publish_before = job.date_published
        expire_before = job.date_expired
        job.publish(settings.MDR_SCORE_MIN)

        njob = Job.objects.get(pk=job.id)
        after_prof = UserProfile.objects.get(user=author)

        publish_after = njob.date_published
        expire_after = njob.date_expired

        self.assertTrue(after_prof.karma > authprof.karma)
        self.assertTrue(publish_after > publish_before)
        self.assertTrue(expire_after > expire_before)
        self.assertEqual(njob.status, settings.JOB_STATUS_PUBLISHED)

    def test_publish_edition(self):
        """Publish a job

        - if the job is in status edition the karma is not updated
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')
        authprof = UserProfile.objects.get(user=author)

        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_EDITION)

        job.publish(settings.MDR_SCORE_MIN)

        after_prof = UserProfile.objects.get(user=author)

        self.assertEqual(after_prof.karma, authprof.karma)
        self.assertEqual(job.status, settings.JOB_STATUS_PUBLISHED)

    def test_moderate_edition(self):
        """Moderate  a job

        - if the job is in status edition moderation is ok
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')

        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_EDITION)
        job.moderate()

        self.assertEqual(job.status, settings.JOB_STATUS_MODERATION)

    def test_moderate_refused(self):
        """Moderate  a job

        - if the job is in status refused moderation is not ok
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')

        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_REFUSED)
        job.moderate()

        self.assertEqual(job.status, settings.JOB_STATUS_REFUSED)

    def test_unpublish_published(self):
        """Unpublish  a job

        - The job must be in status PUBLISHED
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')

        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_PUBLISHED)
        job.unpublish()

        self.assertEqual(job.status, settings.JOB_STATUS_ARCHIVED)

    def test_unpublish_refused(self):
        """Unpublish  a job

        The status is not PUBLISHED so nothing is done
        """
        author = User.objects.create_user('refuse_author',
                                          'admin_search@bar.com',
                                          'admintest')

        job = Job.objects.create(author=author,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_REFUSED)
        job.unpublish()

        self.assertEqual(job.status, settings.JOB_STATUS_REFUSED)

    def test_publish_user_firstvote(self):
        """First vote on a job to publish it
        """
        user = User.objects.create_user('publish_user_first_vote',
                                        'admin_search@bar.com',
                                        'admintest')

        # give to the user the permission to vote
        userprof = UserProfile.objects.get(user=user)
        userprof.jobmoder_allowed = True
        userprof.save()

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=settings.JOB_STATUS_MODERATION)

        job.publish_user(user)  # action

        njob = Job.objects.get(pk=job.id)

        # The job is still in moderation
        self.assertEqual(njob.status, settings.JOB_STATUS_MODERATION)
        self.assertEqual(njob.moderation_votes, 1)
        self.assertEqual(njob.moderation_score, 1)

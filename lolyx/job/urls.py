# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Urls for app : Job
#
from django.conf.urls import patterns, url
from lolyx.job.views import (JobPublicListView, JobModeratorListView,
                             JobArchiveListView, JobPublicView, JobEditView,
                             JobModeratorView, JobPrivateView, JobUserViewList)
from lolyx.job.feeds import LatestJobFeed

urlpatterns = patterns('',
                       url(r'^moderator/$', JobModeratorListView.as_view(), name="jobs_moderators"),
                       url(r'^archives/$', JobArchiveListView.as_view(), name="jobs_archives"),
                       url(r'^(?P<pk>\d+)/stat/view.json$', 'lolyx.job.views.job_stat_json', name='job_stat_view_json'),                       
                       url(r'^(?P<pk>\d+)/bookmark/$', 'lolyx.job.views.bookmark'),
                       url(r'^(?P<pk>\d+)/copy/$', 'lolyx.job.views.copy', name='job_copy'),
                       url(r'^(?P<pk>\d+)/publish/$', 'lolyx.job.views.publish', name='job_publish'),
                       url(r'^(?P<pk>\d+)/refuse/$', 'lolyx.job.views.refuse', name='job_refuse'),
                       url(r'^(?P<pk>\d+)/unpublish/$', 'lolyx.job.views.unpublish', name='job_unpublish'),
                       url(r'^(?P<pk>\d+)/republish/(?P<key>[0-9a-f-]{36})/$', 'lolyx.job.views.republish', name='job_republish'),
                       url(r'^(?P<pk>\d+)/moderate/$', JobModeratorView.as_view(), name='job_moderate'),
                       url(r'^(?P<pk>\d+)/edit/lvltool/(?P<toolid>\d+)/(?P<level>\d)$', 'lolyx.job.actionviews.lvltool'),
                       url(r'^(?P<pk>\d+)/edit/addtool/(?P<toolid>\d+)$', 'lolyx.job.actionviews.addtool'),
                       url(r'^(?P<pk>\d+)/edit/deltool/(?P<jobtoolid>\d+)$', 'lolyx.job.actionviews.deltool'),
                       url(r'^(?P<pk>\d+)/edit/addlang/(?P<langid>\d+)$', 'lolyx.job.actionviews.addlang'),
                       url(r'^(?P<pk>\d+)/edit/dellang/(?P<joblangid>\d+)$', 'lolyx.job.actionviews.dellang'),
                       url(r'^(?P<pk>\d+)/edit/$', JobEditView.as_view(), name="job_edit"),
                       url(r'^(?P<pk>\d+)/$', JobPublicView.as_view(), name="job"),
                       url(r'^viewed/$', JobUserViewList.as_view(), name="jobviewed"),
                       url(r'^feed/latest/$', LatestJobFeed(), name="offres_feed"),
                       url(r'^new/$', 'lolyx.job.views.new', name="job_new"),
                       url(r'^myjobs/$', 'lolyx.job.views.myjobs'),
                       url(r'^stats/(?P<family>emploi|stage|mission)/(?P<indicator>\w+).json$', 'lolyx.job.views.stats'),
                       url(r'^json/emploi$', JobPublicListView.as_view(paginate_by=10)),
                       url(r'^json/stage$', JobPublicListView.as_view(paginate_by=10)),
                       url(r'^json/mission$', JobPublicListView.as_view(paginate_by=10)),
                       url(r'^json/tools/(?P<family>\d)$', 'lolyx.job.views.jobtool_public'),
                       url(r'^$', JobPublicListView.as_view(), name="jobs_public_list"),
                       )

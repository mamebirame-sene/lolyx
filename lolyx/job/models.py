# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models definition for app job
"""
from datetime import datetime, timedelta
import logging
from markdown import markdown
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from djorm_pgfulltext.models import SearchManager
from djorm_pgfulltext.fields import VectorField
from django_hstore import hstore
from lolyx.llx.models import Company, UserProfile, UserPoint
from lolyx.llx.models import Tool, Region, Poste, Lang
from lolyx.job.tasks import mail_moderation, tweet_job, mail_job_is_online
from uuidfield import UUIDField
from vmail.models import Alias


class Job(models.Model):
    """
    The job object

    """
    # the user who create this job
    author = models.ForeignKey(User,
                               on_delete=models.PROTECT)

    # the company who is hire
    company = models.ForeignKey(Company,
                                on_delete=models.PROTECT)

    # the region
    region = models.ForeignKey(Region, blank=True, null=True,
                               on_delete=models.PROTECT)

    # the company who is hire
    fkposte = models.ForeignKey(Poste, blank=True, null=True,
                                on_delete=models.PROTECT)

    # classe
    family = models.IntegerField(default=0,
                                 choices=((0, 'emploi'),
                                          (1, 'stage'),
                                          (2, 'mission')))

    title = models.CharField(max_length=300,
                             verbose_name='Job title',
                             blank=True)

    # Internal reference of the company
    ref = models.CharField(max_length=50,
                           verbose_name='Reference',
                           blank=True)

    description = models.TextField(verbose_name='Description',
                                   blank=True)

    description_markup = models.IntegerField(default=0,
                                             choices=((0, 'text/plain'),
                                                      (1, 'markdown'),
                                                      (2, 'reST')))

    poste = models.CharField(max_length=50,
                             verbose_name='Poste',
                             blank=True)

    salaire = models.CharField(max_length=50,
                               verbose_name='Salaire',
                               blank=True)

    # Le poste ouvert est-il une creation de poste
    creation = models.BooleanField(default=False,
                                   verbose_name='Création de poste')

    # teletravail possible
    teletravail = models.BooleanField(default=False,
                                      verbose_name='Teletravail')

    # job status
    status = models.PositiveSmallIntegerField(default=0,
                                              choices=((0, 'Editing'),
                                                       (1, 'Online'),
                                                       (2, 'Moderation'),
                                                       (3, 'Refused'),
                                                       (4, 'Desactive'),
                                                       (5, 'Deleted')))

    # contact
    contact_name = models.CharField(max_length=50,
                                    verbose_name='Contact',
                                    blank=True)

    contact_email = models.EmailField(max_length=100,
                                      verbose_name='Contact email',
                                      blank=True)

    contact_tel = models.CharField(max_length=30,
                                   verbose_name='Contact tel',
                                   blank=True)

    # nb of times the job was viewed on internet
    viewed = models.PositiveIntegerField(default=0)
    # When the job was created
    date_created = models.DateTimeField(auto_now_add=True)
    # When the job was published online
    date_published = models.DateTimeField(auto_now_add=True)
    # When the job will be unpublished, or when the job was unpublished
    epoch = datetime(1970, 1, 1, 0, 0, 0, 0)
    date_expired = models.DateTimeField(auto_now_add=False,
                                        default=epoch)

    # Does the job compatible with GNU rules
    gnu = models.BooleanField(default=False)

    # moderation score
    moderation_score = models.SmallIntegerField(default=0)
    moderation_votes = models.PositiveSmallIntegerField(default=0)

    # longitude/latitude
    lon = models.FloatField(default=0.0)
    lat = models.FloatField(default=0.0)

    # points used when the job was pushed in moderation
    # used instead with back
    cost = models.SmallIntegerField(default=0)

    # full text search field
    search_index = VectorField()

    objects = SearchManager(
                fields = ('title', 'description'),
                config = 'pg_catalog.french', # this is default
                search_field = 'search_index', # this is default
                auto_update_search_field = True
        )

    def __unicode__(self):
        """
        The unicode method
        """
        return self.title

    def create_vmail_alias(self):
        """Create an alias
        """
        source = self.vmail_alias
        destination = self.contact_email
        alias = Alias.objects.create(domain_id=settings.VMAIL_DOMAIN_ID,
                                     source=source,
                                     destination=destination)
        return alias.id

    def remove_vmail_alias(self):
        """Remove an alias
        """
        source = self.vmail_alias
        Alias.objects.filter(domain_id=settings.VMAIL_DOMAIN_ID,
                             source=source).delete()

    def publish_moderator(self):
        """Publish action made by a user with moderator privileges
        """
        self.publish(settings.MDR_MODERATOR_POINT)

    def publish_user(self, user):
        """Publish action by a user
        """
        has_voted = JobVote.objects.filter(user=user,
                                           job=self).count()
        if has_voted == 0:
            JobVote.objects.create(job=self,
                                   user=user,
                                   vote=settings.MDR_USER_POINT)
            self.publish(settings.MDR_USER_POINT)

    def publish(self, points):
        """
        The job is accepted and published online
        """
        self.moderation_score = self.moderation_score + points
        self.moderation_votes = self.moderation_votes + 1

        if self.moderation_score >= settings.MDR_SCORE_MIN:
            if self.status == settings.JOB_STATUS_MODERATION:
                # Attribute karma points
                profile = UserProfile.objects.get(user=self.author)
                profile.karma = profile.karma + settings.KARMA_JOB_ACCEPTED
                profile.save()
            self.dopublish()

        self.save()

    def dopublish(self):
        """Publish the job
        """
        logger = logging.getLogger(__name__)
        logger.debug('publish job {} status was {}'.format(self.id, self.status))
        self.status = settings.JOB_STATUS_PUBLISHED
        self.date_published = datetime.now()
        self.date_expired = self.date_published + timedelta(days=settings.JOB_ONLINE_LIFETIME)
        self.create_vmail_alias()

        tool_list = {}
        tool_names = []
        tools = JobTool.objects.filter(job_id=self.id).values_list('tool__slug', 'tool__name')
        for tool in tools.order_by('-level'):
            tool_list[tool[0]] = tool[1]
            tool_names.append(tool[1])
        JobOnline.objects.create(job=self, tools=tool_list)
        # publish a tweet
        tweet_job.delay(self, tool_names)
        # inform author by email
        mail_job_is_online.delay(self)
        
    def moderate(self):
        """
        The job is going to moderation queue
        """
        logger = logging.getLogger(__name__)

        if self.status == settings.JOB_STATUS_EDITION:
            logger.debug('moderate job {}'.format(self.id))
            self.status = settings.JOB_STATUS_MODERATION
            self.cost = settings.POINTS_JOB_PUBLISH
            self.save()
            # remove points from user's credits
            userpts = UserPoint.objects.get(user=self.author)
            userpts.remove_points(settings.POINTS_JOB_PUBLISH)
            # launch the mail
            tid = mail_moderation.delay(self)
        else:
            logger.debug('wrong status do not moderate job {}'.format(self.id))

    def republish(self):
        """The job is republished after a first publication
        """
        logger = logging.getLogger(__name__)

        logger.debug('publish job {} status was {}'.format(self.id, self.status))
        self.status = settings.JOB_STATUS_PUBLISHED
        self.date_published = datetime.now()
        self.date_expired = self.date_published + timedelta(days=settings.JOB_ONLINE_LIFETIME)
        self.save()

    def unpublish(self):
        """The job will be archived
        """
        logger = logging.getLogger(__name__)
        logger.debug('unpublish job {}'.format(self.id))

        self.remove_vmail_alias()

        if self.status == settings.JOB_STATUS_PUBLISHED:
            self.status = settings.JOB_STATUS_ARCHIVED
            self.save()
            JobRepublishKey.objects.create(job=self)
        if self.status == settings.JOB_STATUS_MODERATION:
            self.status = settings.JOB_STATUS_EDITION
            self.save()
            # give credits back to the user accounts
            userpts = UserPoint.objects.get(user=self.author)
            userpts.add_points(self.cost)

    def refuse_moderator(self, user):
        """Refuse action made by a moderator
        """
        has_voted = JobVote.objects.filter(user=user,
                                           job=self).count()
        if has_voted == 0:
            JobVote.objects.create(job=self,
                                   user=user,
                                   vote=-settings.MDR_MODERATOR_POINT)

            self.refuse(settings.MDR_MODERATOR_POINT)

    def refuse_user(self, user):
        """Refuse action made by an user
        """
        has_voted = JobVote.objects.filter(user=user,
                                           job=self).count()
        if has_voted == 0:
            JobVote.objects.create(job=self,
                                   user=user,
                                   vote=-settings.MDR_USER_POINT)

            self.refuse(settings.MDR_USER_POINT)

    def refuse(self, points):
        """
        The job is refused and not published online
        """
        logger = logging.getLogger(__name__)
        logger.debug('refuse job {}'.format(self.id))

        self.moderation_score = self.moderation_score - points
        self.moderation_votes = self.moderation_votes + 1

        userpts = UserPoint.objects.get(user=self.author)
        userpts.add_points(self.cost)

        if self.moderation_score >= settings.MDR_SCORE_MIN:
            profile = UserProfile.objects.get(user=self.author)
            profile.karma = profile.karma - settings.KARMA_JOB_REFUSED
            profile.save()

            self.status = settings.JOB_STATUS_REFUSED

            self.company.job_refused()

        self.save()

    def get_absolute_url(self):
        """Return the absolute url
        used with feeds syndication"""
        return "/{}/offres/{}/".format(settings.FAMILY_NAMES[self.family],
                                       self.id)

    def get_edit_url(self):
        """Return the edit  url
        """
        return "/job/{}/edit/".format(self.id)

    @property
    def vmail_alias(self):
        """Adresse publique de l'offre
        """
        vmail = "offre%s@%s" % (str(self.id),
                                settings.VMAIL_ALIAS_DOMAIN)
        return vmail


    @property
    def description_markdown(self):
        """Return description formated in Markdown
        """
        return markdown.markdown(self.description)

    @property
    def in_moderation(self):
        return self.status == settings.JOB_STATUS_MODERATION

    @property
    def is_publish(self):
        return self.status == settings.JOB_STATUS_PUBLISHED


class JobTool(models.Model):
    """Store required tools knowledge
    """
    job = models.ForeignKey(Job)
    tool = models.ForeignKey(Tool,
                             on_delete=models.PROTECT)
    level = models.SmallIntegerField(default=3)
    contrib = models.SmallIntegerField(default=0)
    # in years
    experience = models.SmallIntegerField(default=0)

    # usefull for statistics
    tms = models.DateTimeField(auto_now_add=True,
                               default=datetime.now())

    class Meta:
        unique_together = ('job', 'tool',)
        index_together = [['id', 'tool']]


class JobOnline(models.Model):
    """Lists job actually online

    With a finite list of job online associated with a sequence,
    it is possible to do key pagination
    """
    job = models.ForeignKey(Job)

    # length must be the same as tool slug Field
    tools = hstore.DictionaryField(db_index=False,
                                   blank=True,
                                   null=True)

    objects = hstore.HStoreManager()


class JobLang(models.Model):
    """Store required langs knowledge
    """
    job = models.ForeignKey(Job)
    lang = models.ForeignKey(Lang)
    level = models.SmallIntegerField(default=3)

    # useful for statistics
    tms = models.DateTimeField(auto_now_add=True,
                               default=datetime.now())

    class Meta:
        unique_together = ('job', 'lang',)


class JobStat(models.Model):
    """Store when and who view the job
    """
    job = models.ForeignKey(Job)
    datetime = models.DateTimeField(auto_now_add=True)


class JobUserView(models.Model):
    """Store when and who view the job
    """
    user = models.ForeignKey(User)
    job = models.ForeignKey(Job)
    datetime = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = [['user', 'datetime']]


class JobVote(models.Model):
    """Vote on a job by a user
    """
    user = models.ForeignKey(User)
    job = models.ForeignKey(Job)
    date = models.DateTimeField(auto_now_add=True)
    vote = models.SmallIntegerField()

    class Meta:
        index_together = [['user', 'job']]


class JobRepublishKey(models.Model):
    """A key used to republish a job with one URL
    """
    job = models.ForeignKey(Job)
    key = UUIDField(auto=True)
    generated = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = [['job', 'key']]


class JobBookmark(models.Model):
    """Bookmark a job
    """
    user = models.ForeignKey(User)
    job = models.ForeignKey(Job)
    date = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        """Return the absolute url
        used with feeds syndication"""
        return "/jobbookmark/{}/".format(self.id)

    class Meta(object):
        unique_together = ('user', 'job')
        index_together = [['user', 'date']]

# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib import admin
from lolyx.job.models import Job, JobVote


class JobAdmin(admin.ModelAdmin):
    """
    Custom Admin for jobs
    """
    readonly_fields = ('company', 'author')
    list_display = ('title', 'company')


class JobVoteAdmin(admin.ModelAdmin):
    """
    Custom Admin for job votes
    """
    readonly_fields = ('user', 'job', 'vote')
    list_display = ('user', 'job')


admin.site.register(Job, JobAdmin)
admin.site.register(JobVote, JobVoteAdmin)

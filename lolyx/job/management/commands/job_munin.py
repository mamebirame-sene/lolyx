#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output stats for munin
"""
import json
from django.core.cache import cache
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Count
from lolyx.job.models import Job, JobTool
from lolyx.llx.models import Tool, Region


class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.count(0, 'emploi')
        self.count(1, 'stage')
        self.count(2, 'mission')

    def count(self, family, label):

        labels = ['emploi', 'stage', 'mission']

        all_tools = {}
        stats = {}
        children = []

        nb_job = Job.objects.filter(family=family).count()
        onlines = Job.objects.filter(family=family, status=settings.JOB_STATUS_PUBLISHED)
        moder = Job.objects.filter(family=family, status=settings.JOB_STATUS_MODERATION).count()

        # Print on stdout for munin usage
        self.stdout.write("%s_%s.value %s\n" % ('job', labels[family], nb_job))
        self.stdout.write("%s_%s_online.value %s\n" % ('job', labels[family], len(onlines)))
        self.stdout.write("%s_%s_moderation.value %s\n" % ('job', labels[family], moder))

        # set the cache
        cache.set('job_{}_nb_online'.format(labels[family]), len(onlines), 3600)

        # compute statistics
        for job in onlines:
            jtools = JobTool.objects.filter(job=job).only('tool')
            for jtool in jtools:
                if jtool.tool.pk in all_tools:
                    all_tools[jtool.tool.pk] = all_tools[jtool.tool.pk] + 1
                else:
                    all_tools[jtool.tool.pk] = 1

        for key in all_tools.keys():
            tool = Tool.objects.get(pk=key)

            if tool.category in stats:
                actual = stats[tool.category]
                actual.append({"name": tool.name,
                               "size": all_tools[tool.pk]})
                stats[tool.category] = actual
            else:
                stats[tool.category] = [{"name": tool.name,
                                         "size": all_tools[tool.pk]}]

        for key in stats.keys():
            children.append({"name": key, "children": stats[key]})

        # set global stats
        cache.set('tool_map_{}'.format(label), 
                  json.dumps({"name": "flare", "children": children}), 3600)

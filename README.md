Lolyx
=====

Lolyx is a full rewrite of lolix project, lolix is the software run on
http://fr.lolix.org

AUTHORS
=======

 * Rodolphe Quiedeville <rodolphe@quiedeville.org>

Documentation
=============

* http://doc.lolix.org/

Developpers
===========

Init an instance
================

``rm /tmp/lolyx.sqlite  ; ./manage.py syncdb --noinput; ./manage.py loaddata lolyx/llx/fixtures/dev_data.json``


CI
==

Lolyx is developped under CI process with jenkins the project :

* http://jenkins.quiedeville.org/job/Lolyx

Please open issues on gitlab

Require
=======

See requirements.txt

Debian
------

 * python-requests
 * python-django
 * python-django-celery
==========
Moderation
==========

Utilisateur
-----------

Un utilisateur peut participer à la modération des offres dès que
celui-ci a mis en ligne un CV. Initialement il n'est pas possible de
voter le droit de vote s'acquiert dès la publication d'un CV, si
l'utilisateur n'a pas été blacklisté au préalable.

Un utilisateur ayant le droit de vote a de fait accès aux offres en
attente de modération et reçoit si il l'a demandé un email pour être
informé lors de la mise en modération d'une offre.

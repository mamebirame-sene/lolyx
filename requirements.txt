#
# Required packages to run Lolyx
#
Django==1.6.11
feedparser==5.1.3
celery==3.1.16
django-celery==2.5.5
django-markup==0.4
django_compressor==1.4
django-allauth==0.19.1
django-extensions==1.5.1
Markdown==2.6.1
Pillow==2.7.0
python-memcached==1.53
django-crispy-forms==1.4.0
django-picklefield==0.3.1
django-appconf==0.6
django-uuidfield==0.5.0
python-dateutil==2.4.1
kombu==3.0.24
#
django-endless-pagination==2.0
#
requests==2.6.0
requests-oauthlib==0.4.2
oauthlib==0.7.2
#
django-json-dbindex==1.1.1
django-messages==0.5.1
TwitterAPI==2.3.3

# Packages ony used in plugins
#
launchpadlib
# need libxslt1-dev under Debian to be installed
lxml==2.3.2
BeautifulSoup==3.2.1
django-faker==0.2
#
# Packages are purposed to Developers
#
south
#
# Packages for running tests
#
nosexcover
django-nose
pep8
flake8==2.4.0
#
#  If you use jenkins you can add
#
django-jenkins
gcovr
django-vmail==0.2.3
